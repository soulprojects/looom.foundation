import * as React from "react"
import { Link, graphql } from "gatsby"

import Layout from "../components/layout"
import Seo from "../components/seo"
import logo from "../images/looom-logo.png"
import { navigate } from "gatsby"

const BlogIndex = ({ data, location }) => {
  const siteTitle = data.site.siteMetadata?.title || `Title`
  const projects = data.allAirtable.edges

  React.useEffect(() => {
    if (typeof window !== `undefined`) {
      navigate("/kehalani")
    }
  }, [])

  return (
    <>
      <Seo title="Looom Foundation" />
      <div className="looom__home">
        <div className="splash center">
          <img src={logo} className="logo" />
          <p className="bio">
            looom is an emerging innovative community project rooted in the
            spirit of the land and waters of Kaua’i, where people from all walks
            of life may experience natural birthing, integration of life
            changes...and transitioning onward.
          </p>
          {/* <p className="bio">
            There is one thing we all have in common. No matter who we are,
            where we’re from, or what we look like,{" "}
            <b>we are all here to navigate the journey of living, and dying</b>.
            Yet uniquely at this time and in this culture, we have become
            disconnected from the experience of death, and therefore the full
            experience of life.
            <br />
            <br />
            That’s why we are building looom:{" "}
            <b>
              an integrated and holistic land project where people from all
              walks of life may experience natural birthing, integration of life
              changes, and ultimately transitioning onward
            </b>
            . These three unique properties are nestsle along the river banks of
            Kaua’i, Hawaii, and provide a safe and sacred space for beings to
            connect with indigenous practices of birthing, living, and dying,
            and experience firsthand what it means to harmonize with the cycels
            of nature.
          </p> */}
        </div>
        <div className="projects center">
          <div className="container projects__container center">
            {projects.map(project => (
              <Link to={project.node.data.slug} className="project">
                <div>
                  <div
                    className="project__img"
                    style={{
                      background: `url(${project.node.data.cover?.[0]?.url})`,
                    }}
                  ></div>
                  <h2>{project.node.data.Name}</h2>
                </div>
              </Link>
            ))}
          </div>
        </div>
      </div>
    </>
  )
}

export default BlogIndex

export const pageQuery = graphql`
  query {
    allAirtable(filter: { table: { eq: "Looom" } }) {
      edges {
        node {
          data {
            Name
            stage
            slug
            cover {
              url
            }
            Humans {
              data {
                Name
                Attachments {
                  url
                }
              }
            }
          }
        }
      }
    }
    site {
      siteMetadata {
        title
      }
    }
  }
`
