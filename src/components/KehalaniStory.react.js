import * as React from "react"

import cover from "../images/kehalani.jpg"

const KehalaniStory = () => {
  return (
    <div className="kehalani_story">
      <div className="container">
        <h2>The Becoming of Kehalani </h2>
        <p>
          <blockquote>
            As I sat by the river and asked in my heart, “What is this place?” I
            heard the word clearly in my head, “Hospice.”
          </blockquote>
          <br />
          To answer the calling of the land, we are creating a a tranquil
          healing sanctuary for family members and friends to be present with
          their loved one in their last days of life.
          <br />
          <br />
          In the recent years, more and more people die in isolation, spending
          their last moments in a drug-induced fog. In fact, 80% of Americans
          would prefer to die at home, and yet, only 20% have the opportunity to
          experience that.
          <br />
          <br />
          <img src={cover} />
          <br />
          <br />
          Imagine a place where people in their dying process are surrounded by
          serene nature, the grace of flowing water, and the song of a
          waterfall, while being tended by a team of experienced caregivers.{" "}
          <br />
          <br />
          <blockquote className="red">
            “It is time for a safe space to honor the sacredness of death.”
          </blockquote>
          <br />
          As I write this, a rainbow has appeared. Kehalani is a place that
          carries beings beyond ordinary imagining. We are calling in heartfelt
          support, connection, and participation for the vision of Kehalani to
          shine.
          <br />
          <br />
          Mahalo nui loa
          <br />
          <br />
          Kelly Kelsey
        </p>
      </div>
    </div>
  )
}

export default KehalaniStory
