import * as React from "react"
import handHealing from "../images/healing.svg"
import medicine from "../images/essential-oil.svg"
import music from "../images/herb.svg"

const KehalaniWhat = () => {
  return (
    <div className="kehalani_what">
      <div className="container">
        <h2>A conscious hospice with a network of healers and services</h2>

        <div className="flex">
          <div className="flexItems center">
            <img src={handHealing} alt="React Logo" />
            <h4>Healing Touch</h4>
          </div>
          <div className="flexItems center">
            <img src={medicine} alt="React Logo" />
            <h4>Sound Healing</h4>
          </div>
          <div className="flexItems center">
            <img src={music} alt="React Logo" />
            <h4>Integrative Medicine</h4>
          </div>
        </div>
      </div>
    </div>
  )
}

export default KehalaniWhat
