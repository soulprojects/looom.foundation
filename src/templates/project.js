import * as React from "react"
import { Link, graphql } from "gatsby"

import KehalaniWhat from "../components/KehalaniWhat.react"
import KehalaniStory from "../components/KehalaniStory.react"
import Seo from "../components/seo"

import video from "../images/IMG_6232.mp4"

const ProjectPageTemplate = ({ data, location }) => {
  const project = data.airtable.data
  const siteTitle = project.Name

  console.log(project)

  const backgroundImage = `url(${project.cover?.[0]?.url})`
  const videoUrl = project.video?.[0]?.url

  const [muted, setMuted] = React.useState(true)

  return (
    <>
      <Seo title={siteTitle} />
      <div className="project__page">
        {videoUrl != null ? (
          <div className="project__cover__video">
            <video
              autoPlay={true}
              muted={muted}
              onClick={() => setMuted(false)}
              loop={true}
              playsInline={true}
            >
              <source src={video} type="video/mp4" />
            </video>
          </div>
        ) : (
          <div className="project__cover" style={{ backgroundImage }}></div>
        )}
        <div className="project__header container">
          <h1>{project.Name}</h1>
          <div
            dangerouslySetInnerHTML={{
              __html: project.Tagline.childMarkdownRemark.html,
            }}
          ></div>
          <div
            dangerouslySetInnerHTML={{
              __html: project.details.childMarkdownRemark.html,
            }}
          ></div>

          {/* TODO: Make pills into a component */}
          {/* <div className="pills">
          {project.themes.map(theme => (
            <div className="pill" key={theme}>
              {theme}
            </div>
          ))}
        </div> */}
        </div>

        {/* {project.slug === "kehalani" && <KehalaniWhat />} */}

        <div className="project__people">
          <div className="container">
            <h2>Supporters</h2>
            <div className="people">
              {project.Humans.map(human => (
                <div className="person" key={human.id}>
                  <img src={human.data.Attachments?.[0]?.url} />
                  <h4>{human.data.Name}</h4>
                  <span>{human.data.kehalani_role}</span>
                </div>
              ))}
            </div>
            <p>... and many others in the community</p>
          </div>
        </div>

        <div className="help">
          <div className="container">
            <h2>Help us weave a web of support</h2>
            {project.slug === "kehalani" && (
              <p>
                We invite you to join us in creating a legacy of compassionate
                holistic death-care. We welcome ongoing funding towards Kehalani
                House maintenance and caregiving team to provide the fulfillment
                of care and most beautiful death support center possible.
                <br />
                <br />
                Are you interested in weaving this dream with us? If you’d like
                to become a supporter, please reach out via{" "}
                <a href={`mailto:${project.email}`}>{project.email}</a>
              </p>
            )}
            {project.capital_needed != null && project.capital_raised != null && (
              <div className="captial">
                <div
                  className="capital_raised"
                  style={{
                    width: `${
                      (project.capital_raised / project.capital_needed) * 100
                    }%`,
                  }}
                ></div>
                <div className="capital_text">
                  <center>
                    <h4>
                      ${project.capital_raised} raised / $
                      {project.capital_needed} goal
                    </h4>
                  </center>
                </div>
              </div>
            )}
            <a
              className="button"
              href="https://airtable.com/shr21OL7Q0UMHTJ9x"
              target="_blank"
            >
              Support
            </a>
            <a
              className="button"
              href="https://airtable.com/shr1amTGqfuhfMG9R"
              target="_blank"
            >
              Nominate a friend to help
            </a>
          </div>
        </div>

        {project.slug === "kehalani" && <KehalaniStory />}

        <div className="footer">
          <div className="container">
            <p>A looom foundation initiative</p>
            <p className="small">Contact us at projects@looom.foundation</p>
          </div>
        </div>
      </div>
    </>
  )
}

export default ProjectPageTemplate

export const pageQuery = graphql`
  query ProjectBySlug($id: String!) {
    site {
      siteMetadata {
        title
      }
    }
    airtable(id: { eq: $id }, table: { eq: "Looom" }) {
      id
      data {
        Name
        slug
        email
        Tagline {
          childMarkdownRemark {
            html
          }
        }
        first_line
        details {
          childMarkdownRemark {
            html
          }
        }
        themes
        stage
        Super_Powers
        cover {
          url
        }
        video {
          url
        }
        Humans {
          id
          data {
            Name
            kehalani_role
            Attachments {
              url
            }
          }
        }
        manifestations {
          data {
            Name
            description
          }
        }
        capital_raised
        capital_needed
      }
    }
  }
`
