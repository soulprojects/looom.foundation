const path = require(`path`)
const { createFilePath } = require(`gatsby-source-filesystem`)

exports.createPages = async ({ graphql, actions, reporter }) => {
  const { createPage } = actions

  // Define a template for blog post
  const projectPage = path.resolve(`./src/templates/project.js`)

  // Get all markdown blog posts sorted by date
  const result = await graphql(
    `
      {
        allAirtable(filter: { table: { eq: "Looom" } }) {
          edges {
            node {
              id
              data {
                slug
              }
            }
          }
        }
      }
    `
  )

  if (result.errors) {
    reporter.panicOnBuild(
      `There was an error loading your blog posts`,
      result.errors
    )
    return
  }

  const pages = result.data.allAirtable.edges

  // Create sub pages

  if (pages.length > 0) {
    pages.forEach((page, index) => {
      createPage({
        path: page.node.data.slug,
        component: projectPage,
        context: {
          id: page.node.id,
        },
      })
    })
  }
}

exports.onCreateNode = ({ node, actions, getNode }) => {
  const { createNodeField } = actions

  // if (node.internal.type === `MarkdownRemark`) {
  //   // console.log(node, getNode)
  //   const value = createFilePath({ node, getNode })
  //   console.log(value)

  //   createNodeField({
  //     name: `slug`,
  //     node,
  //     value,
  //   })
  // }
}

exports.createSchemaCustomization = ({ actions }) => {
  const { createTypes } = actions

  // Explicitly define the siteMetadata {} object
  // This way those will always be defined even if removed from gatsby-config.js

  // Also explicitly define the Markdown frontmatter
  // This way the "MarkdownRemark" queries will return `null` even when no
  // blog posts are stored inside "content/blog" instead of returning an error
  createTypes(`
    type SiteSiteMetadata {
      author: Author
      siteUrl: String
      social: Social
    }

    type Author {
      name: String
      summary: String
    }

    type Social {
      twitter: String
    }

    type MarkdownRemark implements Node {
      frontmatter: Frontmatter
      fields: Fields
    }

    type Frontmatter {
      title: String
      description: String
      date: Date @dateformat
    }

    type Fields {
      slug: String
    }
  `)
}
